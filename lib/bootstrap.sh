#!?bin/bash

function bsfail() {
  echo "Bootstrap failed! Look for details in log/bootstrap.log"
  exit 1
}

function bsrun() {
  echo "[`date +%T`] Running '$@'"
  $@ 2>&1 >>log/bootstrap.log || bsfail
}

function bsread() {
  echo -n "$2: "
  read $1
  if [[ -z "${!1}" ]]; then
    echo "$2 is required."
    bsfail
  fi
}
