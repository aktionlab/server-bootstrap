#!/bin/bash

. lib/bootstrap.sh

function apt_install() {
  bsrun sudo apt-get install -y $@
}

function apt_update() {
  bsrun sudo apt-get -y update
}
