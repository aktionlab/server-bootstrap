#!/bin/bash

. lib/bootstrap.sh

function my_ip () {
  echo `ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{ print $1 }'`
}

function add_user() {
  if [ ! -d /home/$1 ]; then
    bsrun sudo adduser --gecos "" --disabled-password $1
  fi
}

function mkdir_p() {
  [[ -z "$@" ]] && return 1

  for dir in $@; do
    bsrun sudo mkdir -p $dir
  done
}
