# Bootstrap

Run the bootstrap script to get the repo initialized with the rest of the setup scripts.

    curl https://bitbucket.org/aktionlab/server-bootstrap/raw/f965134c5d702c2461f477cb85268e340b206fec/bootstrap.sh | sh

To update the scripts to the latest version, simply pull from git

    git pull
