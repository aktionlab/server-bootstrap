#!/usr/bin/bash

echo "" >/tmp/bootstrap.log

echo "Upgrading APT"
sudo apt-get update 2>&1 >>/tmp/bootstrap.log
sudo DEBIAN_FRONTEND=noninteractive apt-get upgrade -y 2>&1 >>/tmp/bootstrap.log

echo "Installing GIT"
sudo apt-get install git -y 2>&1 >>/tmp/bootstrap.log

git clone https://bitbucket.org/aktionlab/server-bootstrap.git

cd server-bootstrap

echo "Bootstrap complete, running setup"

bin/setup
